<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE component PUBLIC "-//JWS//DTD WileyML 20060316 Vers 2.1 WileyML//EN" "../DTD/medhand.dtd">
<html>
<body lang="EN-US" link="blue" vlink="purple">
<div class="Section1">

<p><b>Police</b> usually brings patients to hospital under Section 136 of the Mental Health Act in order for them to be assessed. Police can only use Section 136 to remove someone from <u>a place to which the public have access to </u>(not their own home) to take him / her to <u>a place of safety</u> (hospital or police station). In the case of the patient being brought to Goodmayes hospital for assessment (due to mental health concerns), the police should inform the Bleep Holder of their intention to do so, who will direct them to one of the Section 136 suites. It is the Bleep Holder’s role to establish if Goodmayes Section 136 suites are the best place for an initial assessment to take place or if the patient should be directed elsewhere (i.e. A&amp;E; more secure environment). It is also the <b>Bleep Holder’s</b> role to inform the Duty Doctor, the <b>Home Treatment Team - Acute Assessment Team (HTT - ACAT), and duty Approved Mental Health Professional (duty AMHP)</b> that a patient under Section 136 is on his/her way and to also inform the Duty Doctor, HTT - ACAT and Duty AMHP as soon as the patient has arrived, in order for an assessment to take place as soon as possible. It would be important for the Bleep Holder to check if the patient has a past history of mental illness and communicate this information to all parties involved. </p>
<p>Section 136 is not an admission section. It is solely a section under the MHA whereby police can take someone who meets the criteria for its use from a public place to a place of safety for <b><u>assessment</u></b>. Section 136 is valid for <b>72 hours</b>. During this time a comprehensive assessment must be completed and a clinical decision made whether to rescind the Section or whether a recommendation for a further Section under the MHA should be made. Depending on the outcome of the assessment a patient is discharged home (with / without support from HTT / other community support) or admitted to hospital (informally or subject to another Section of the MHA). The 72-hour period starts as soon as the patient is accepted at the Section 136 suite. NELFT expects that a comprehensive assessment and final decision of care be in place within <b>6 hours</b> of the patient arriving at the Section 136 suite. The Bleep Holder must complete a <b>Datix</b> incident form in the case of a full assessment and final decision of care not being in place after 12 hours of the patient arriving at hospital, and all efforts should be made to get a final management plan in place as soon as possible.</p>
<p>Due to the time scales mentioned above, the initial assessment should take place as soon as possible with at least the duty doctor and HTT - ACAT in attendance. The assessment should not be delayed by waiting for the duty AMHP to arrive. The duty AMHP’s involvement will very much depend on the outcome of the initial assessment. The duty AMHP must be involved in all cases where patients have a history of mental disorder or when mental disorder is identified through the initial assessment. </p>
 
<p><u>Out of hours: </u></p>
<p>The Duty Doctor must inform the <b>on-call ST Doctor</b> of the pending Section 136 assessment, patient’s arrival and the outcome of the initial assessment. It is expected that the Duty Doctor and HTT - ACAT will undertake a comprehensive assessment where possible, and make a detailed documentation of their assessments, findings, and the rationale for their actions.  <u> The duty doctor must keep the on-call ST doctor updated throughout the whole process. </u></p>
 <p><u>During working hours:</u></p>
 <p>There is not an on-call ST Doctor or on-call Consultant during working hours. If the duty doctor needs advise any time during the assessment he/she should seek advice from the Inpatient Consultant / Staff Grade Doctor on the ward to which the patient potentially could be admitted to, or seek advice from Consultants / Staff Grades in the relevant HTT. The consultant / staff grade involved should make the first medical recommendation if the patient needs admission to hospital under the MHA.  </p>
 <p>Patient and staff safety is paramount. Assessments should always take place together with another member of staff, and in an environment deemed safe for the patient and for staff.</p>
 <p><symbol width="640" height="98" src="2.jpg"></symbol>></p>
 <p>An entry must be made in the <b>Progress Notes</b> to reflect that an assessment took place, who was present, and where in RiO information has been documented, what the findings were, and what actions were taken. The <b>Core Assessment</b> needs to be completed with the history, Mental State Examination, and Physical Examination. The <b>Risk Assessment</b> needs to be updated. A summary should also be made in the <b>Core Assessment</b> highlighting all the positive findings.</p>
<p><symbol width="640" height="98" src="3.jpg"></symbol></p>
<p>The Duty Doctor should at all times consider if a patient is <b>medically fit </b>to remain under the care of mental health services, or if they should be transferred to a general district hospital for medical assistance, especially where overdose is suspected or confirmed or when abnormalities are detected during the physical examination. King George Hospital A&amp;E is the most practical option for a patient to be transferred to. In cases where police have used a Taser on a patient, the patient needs to be medically cleared by A&amp;E or - as agreed with LAS – by an ambulance paramedic whilst on route to the hospital.</p>
<p>Once the initial assessment is complete the Duty Doctor and HTT – ACAT must make an agreed decision as to whether:</p>
<ol>
<li>	The patient is showing signs of a mental disorder / has a history of mental disorder?,   </li>>
<li> 	If the patient needs to be admitted to hospital?,   </li>>
<li>	What community support needs to be organised if discharged from hospital?  </li>>
</ol>
<p> The Duty Doctor must keep the on-call ST Doctor updated throughout the whole process, and especially seek advice if the Duty Doctor is unsure or there is disagreement as to the outcome and proposed action of the assessment between the Duty Doctor, and/or HTT - ACAT, and/or AMHP (on either the above mentioned 3 points). The on-call ST doctor will decide as to whether they give telephonic advice or decide to come and assess the patient in person. If there remains disagreement following input from the <b>on-call</b> ST doctor, then the on-call Consultant needs to be contacted for an opinion. If during working hours, advice should be sought as indicated above.
</p>
<p><u> Possible outcomes:</u></p>

<ol>
 <li> 	If it is agreed that the patient <u>clearly does not show signs of a mental disorder and does not have a history of mental disorder</u> - then the patient can be discharged from Section 136 of the MHA by completing the Section 136 form (after discussing with the on-call ST Doctor). <b>If it is felt to be helpful, and the patient agrees, they may be asked to stay until an AMHP assesses, but cannot be made to do so.</b> The Duty Doctor writes to the patient’s GP to inform of the reason for assessment, details of the assessment and the outcome of the assessment. The patient is informed of the outcome, and given information of other support networks in the community (i.e. counselling / psychological services, Access and Assessment Teams, drug and / alcohol advisory services, etc.).  The team needs to ensure the patient’s safe onward conveyance back into the community and any required follow-up. The Bleep Holder informs the Police of the outcome. </li>
 <li>	If it is <u>uncertain / it is clear that the patient has a history of mental disorder</u> and/or suffers from a mental disorder – then the Duty Doctor must seek advice from the on-call ST Doctor. Risk Assessment is very important in this situation as this will help to determine if the patient can be safely managed and monitored in the community by HTT or if the patient needs to be admitted to hospital in order to explore and observe their mental state further (as an informal patient or a patient subject to the MHA). <b>The duty AMHP must be involved in this situation and be part of the decision making process. </b>
 <ul>
  <li> If it is agreed by all involved that the patient can be managed safely in the community - then the patient can be discharged from Section 136 of the MHA by completing the Section 136 form (after discussing with the on-call ST Doctor). The patient is informed of the outcome, and given information of other support networks in the community (i.e. referral to counselling / psychological services, Access and Assessment Teams, drug and / alcohol advisory services, etc.). A clear decision needs to be made if HTT will be providing further follow-up or not. The team needs to ensure the patient’s safe onward conveyance back into the community and any required follow-up. The Duty Doctor writes to the patient’s GP to inform of the reason for assessment, details of the assessment and the outcome of the assessment. The Bleep Holder informs the Police of the outcome. </li>
  <li> If it is agreed that the patient needs admission to hospital for further observation / assessment – This decision needs to be discussed with the patient. If the patient agrees to admission and has the capacity to do so they can be admitted informally.  </li>>
  
</ul>
</li>
</ol>

<p>If the patient refuses admission, or lacks capacity to make a decision, then the on-call ST Doctor needs to undertake an assessment under the MHA together with the duty AMHP.</p>
<p><b> NOTE: </b> If the Duty Doctor is Section 12 approved they can continue with the assessment under the Mental Health Act together with the duty AMHP.</p>

<p><b><u>Following assessment by the on-call ST Doctor:</u></b></p>

<p><u>In the event that it is decided that the patient does not need admission to hospital:</u></p>
<p>The patient is informed of the outcome. The on-call ST Doctor documents the assessment, discussions and outcome. The on-call ST Doctor also documents whether the patient will receive input from HTT or not, as well as any other follow-up (i.e. care coordinator, referral to counselling / psychological services, Access and Assessment Teams, drug and / alcohol advisory services, etc.). The on-call ST Doctor writes to the patient’s GP to inform of the reason for assessment, details of the assessment and the outcome of the assessment. The team needs to ensure the patient’s safe onward conveyance back into the community and any required follow-up. The Bleep Holder informs the Police of the outcome.</p>
<p><u>In the event that it is decided that the patient needs admission to hospital: </u></p>

<p>This decision needs to be discussed with the patient. If the patient agrees to admission and has the capacity to do so they can be admitted informally. If the patient does not agree admission, a decision needs to be made whether an application for admission under the Mental Health Act needs to be made. If the patient lacks capacity and is not verbally or physically objecting to admission, then they can be admitted under Section 5 of the Mental Capacity Act (MCA) in their best interest, with consideration given to Deprivation of Liberty Safeguards (DoLS). The reasons / rationale for the use of the MCA need to be clearly documented. </p>
<p>  If the patient is admitted to the ward, the Duty Doctor together with the admitting nurse must complete the full admission clerking on the dedicated ward. The on-call ST Doctor should advise the Duty Doctor and ward staff on the management plan for the patient.</p>
<p>On occasions when both the Section 136 suites are occupied and another Section 136 is brought to hospital, it is the role of the Bleep Holder to decide which patient will be taken to a designated acute ward for assessment. The designated acute ward will host the patient whilst an assessment is undertaken. Ward staff will fulfil the role of the Bleep Holder once the patient arrives on the ward.</p>
<p>Please see Section 136 flowchart on next page.
For more information please refer to the Section 136 policy.
</p>
<p><b> <u> SECTION 136 FLOWCHART</u></b></p>
<img width="640" height="98" src="1.jpg"></img>

<p>Section 5(2) is a holding power under the Mental Health Act and it <b> can only be used when a patient </b> has already been officially admitted to hospital as an informal patient. It lasts for <b>72 hours </b>, during which time further assessments should take place in order to determine if the Section should be rescinded or if an application for Section 2 / 3 under the MHA should be made.</p>
<p><u>During working hours:</u></p>
<p>If an informal patient requests to be discharged from the ward, then nursing staff contacts the dedicated ward Consultant to discuss the use of Section 5(2). It is up to the ward Consultant as the Responsible Clinician (RC) to:</p>
<ol>
<li>	Assess the patient themselves for Section 5(2), or  </li>
<li> 	To formally nominate a member of their medical team to undertake the Section 5(2) assessment (must be documented by the ward Consultant), or</li>
<li> 	Request the Duty Doctor to act as the Appointed Nominee of the Responsible Clinician (RC) to undertake the Section 5(2) assessment.</li>

</ol>

<p>If the ward consultant is on leave then nursing staff / rest of the medical team should contact the covering Consultant (who acts as the RC) to decide which action to be taken.</p>
<p>If the ward Consultant or covering Consultant cannot be contacted, then the assessment automatically falls to the Duty Doctor to act as the Appointed Nominee of the Responsible Clinician (RC) to undertake the Section 5(2) assessment.</p>

<p><b><u>Out-of-hours:</u></b></p>
<p>If an informal patient requests to be discharged from the ward, then nursing staff contacts the Duty Doctor to act as the Appointed Nominee of the Responsible Clinician (RC) to undertake the Section 5(2) assessment. </p>
<p><b><u>Assessment:</u></b></p>
<ul>
<li> Discuss the patient’s recent presentation with nursing staff, </li>
<li> Consult the recent progress notes,</li>
<li> Look at the risk assessment, </li>
<li> Look at other collateral evidence,</li>
<li> Carry out a mental state examination and a risk assessment,</li>
<li> Seek advice from senior medical colleagues if needed.</li>
</ul>
<p> <b><u>Outcome:</u></b></p>
<ol>
<li>The Duty Doctor’s first option would be to try and encourage the patient to remain on the ward as an informal patient in order to be seen by the ward medical team the next day / at the next ward review. The Duty Doctor <b>must</b> be satisfied that the patient agrees to such an arrangement and understands this. </li>
<li>	Alternatively the Duty Doctor could consider sending the patient on leave from the ward (if it is safe to do so) with support from the HTT, in order for the patient to return to the ward on the first working day in order to be reviewed by the ward medical team. This option <b>must</b> be supported by the nursing staff on the ward and HTT, and <b>must</b> be discussed with the on-call ST Doctor. </li>
<li>	The Duty Doctor could also consider discharging the patient from the ward (if it is safe to do so). This option <b>must</b> be supported by the nursing staff on the ward and HTT and <b>must</b> be discussed with the on-call ST Doctor. A “Discharge Against Medical Advice” form should be completed. The patient <b>must</b> have the capacity to sign this form and understand the risks involved in their decision to be discharged from the ward.</li>
<li>	If the Duty Doctor concludes that the patient has a mental disorder, and is a potential risk to themselves or others or to their health, (and none of the above options are suitable), the Duty Doctor can place the patient under Section 5(2) of the MHA by completing Form H1. The Doctor <b>must</b> fully complete From H1, giving evidence of the reasons why Section 5(2) is appropriate, sign and date the form, and give the form to the Shift Coordinator on the ward, who <b>must</b> also sign the form to signify acceptance of the Section, and the start of the 72-hour period. </li>
</ol>
<p>If the patient has been placed on Section 5(2), the Duty Doctor and Shift Coordinator should ensure that the patient is assessed by the patient’s own team within 24 hours. If that is not possible (because it is at the weekend), the <b> Duty Doctor </b> should inform the <b>on-call </b> ST Doctor to undertake an assessment and determine if the patient needs to be placed under Section 2 / 3 of the MHA. The <b>Shift Coordinator</b> should inform the <b>duty AMHP </b> of implementation of Section 5(2). The on-call ST doctor liaises with the duty AMHP to inform them of the outcome of their assessment i.e. section 5(2) revoked or first recommendation for Section 2 / 3 made. </p>

<p>For more detail please see the Trust Policy on Section 5(2) use.</p>
<p>In instances when the Duty Doctor is dealing with another emergency and will not be able to see the patient fairly quickly, there is the option for a qualified nurse to use Section 5(4) to restrict the patient from leaving the ward. Section 5(4) however only lasts for 6 hours, and gives limited time for further assessments to take place. The Duty Doctor can however implement a Section 5(2) if further time is needed to arrange a MHA assessment.</p>
<p><b>Dr Vincent Perry</b></p>
<p><b>(AMD for Inpatient and Acute Services)</b></p>
<p><b>April 2016</b></p>
</div></body></html>